import java.util.ArrayList;

public class DebugThis {

    private static ArrayList<Integer> fibonacciMap = new ArrayList<Integer>(2);
    private static int fibonacci(int i) {
        if (i >= fibonacciMap.size()) {
            int upSize = Math.max(i,fibonacciMap.size());
            fibonacciMap.ensureCapacity(upSize);
            for (int j = fibonacciMap.size(); j < upSize; j++) {
                fibonacciMap.add(fibonacciMap.get(j-2) + fibonacciMap.get(j-1));
            }
        }
        return fibonacciMap.get(i);
    }

    public static void main(String cheese[]) {
        // initialize
        fibonacciMap.add(0, 0);
        fibonacciMap.add(1, 1);
        // test
        int one = fibonacci(1);
        assert one == 1;
        int thirteen = fibonacci(7);
        assert thirteen == 13;
    }
}
