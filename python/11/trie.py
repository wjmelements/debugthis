_root = {}

TERMINAL = "\0"

def insert(whole):
    def _insert(root, prefix):
        if not prefix:
            root[TERMINAL] = True
            return
        first = prefix[0]
        if first not in root:
            root[first] = {}
        _insert(root[first], prefix[1:])
    _insert(_root, whole)

def find(prefix):
    def _find(root, prefix):
        if prefix:
            return _find(root[prefix[0]], prefix[1:])
        return root
    return _find(_root, prefix)

def delete(prefix):
    def _delete(root, prefix):
        if not prefix:
            return
        first = prefix[0]
        if first not in root:
            return
        _delete(root[first], prefix[1:])
        del root[first]
    _delete(_root, prefix)

def get_all(prefix):
    def _get_all(root):
        suffixes = []
        for a in root:
            if a is TERMINAL:
                suffixes.append("")
                break
            for item in _get_all(root[a]):
                suffixes.append(a + item)
        return suffixes
    return [prefix + suffix for suffix in _get_all(find(prefix))]

def run_unit_tests():
    insert("Hello, World!")
    assert get_all("") == ["Hello, World!"]
    insert("Hell World")
    assert get_all("Hello") == ["Hello, World!"]
    assert len(get_all("Hell")) == 2

    insert("cat")
    insert("car")
    insert("card")
    insert("cord")
    assert len(get_all("")) == 6
    delete("car")
    insert("car")
    assert len(get_all("")) == 6

if __name__ == "__main__":
    run_unit_tests()
