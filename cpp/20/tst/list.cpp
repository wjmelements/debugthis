#include <assert.h>
#include "list.h"

void basic() {
    list<int> l1;
    assert(!l1.get().valid);
    l1.put(1);
    assert(l1.get() == 1);
    assert(!l1.get().valid);
    l1.put(1);
    l1.put(2);
    l1.put(3);
    assert(l1.get() == 1);
    assert(l1.get() == 2);
    assert(l1.get() == 3);
    assert(!l1.get().valid);
    for (int j = 0; j < 100; j++) {
        for (int i = 0; i < j; i++) {
            l1.put(i);
        }
        for (int i = 0; i < j; i++) {
            assert(l1.get() == i);
        }
        assert(!l1.get().valid);
        for (int i = j; i < 100; i++) {
            l1.put(i);
            assert(l1.get() == i);
        }
        assert(!l1.get().valid);
    }
}

int main() {
    basic();
    return 0;
}
