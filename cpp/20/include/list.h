#include "optional.h"
#ifndef NULL
#define NULL 0
#endif

template<typename T>
struct list {
    list() {
        this->last = &this->first;
        this->first = NULL;
    }
    ~list() {
        while (this->first) {
            node *toFree = this->first;
            this->first = toFree->next;
            delete toFree;
        }
    }
    void put(T value) {
        this->last = &(*this->last = new node(value))->next;
    }
    optional<T> get() {
        optional<T> got;
        if (!this->first) {
            got.valid = false;
        } else {
            got.value = this->first->value;
            got.valid = true;
            node *toFree = this->first;
            this->first = this->first->next;
            delete toFree;
        }
        return got;
    }
private:
    struct node {
        node *next;
        T value;
        node(T value) {
            this->value = value;
            this->next = NULL;
        }
    } *first, **last;
};
