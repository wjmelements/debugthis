#include <assert.h>
template<typename T>
struct optional {
    T value;
    bool valid;

    bool operator==(T value) {
        assert(valid);
        return this->value == value;
    }
};
