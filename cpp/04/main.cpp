#include "assert.h"
#include "stdlib.h"
#include "stdio.h"

#ifndef NULL
#define NULL 0
#endif

#include <map>
using std::map;

#define likely(x) __builtin_expect(x, 1)
#define unlikely(x)__builtin_expect((x),0)

/*
    Returns the ith fibonacci number
    Memoized for great efficiency.
*/
static map<unsigned int,unsigned int> _fib;
unsigned int fib(unsigned int i) {
    if (unlikely(_fib.count(i) == NULL)) {
        if (_fib.count(i-2) == NULL) {
            fib(i-2);
        } else {
            fib(i-1);
        }
        unsigned int sum = _fib[i-1] + _fib[i-2];
        _fib[i] = sum;
        return sum;
    } else {
        return _fib[i];
    }
}
int main() {
    // initialize
    _fib[0] = 0;
    _fib[1] = 1;
    // easy test cases
    assert(fib(0) == 0);
    assert(fib(1) == 1);
    // moar tests
    for (unsigned int i = 2; i < 8; i++) {
        assert(_fib[i] + _fib[i+1] == fib(i+2));
    }
    assert(fib(20) == 6765);
}
