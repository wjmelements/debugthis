#include "assert.h"
#include "stdlib.h"
#include "stdio.h"

#ifndef NULL
#define NULL 0
#endif

#include <map>
using std::map;
/*
    Returns the ith fibonacci number
    Memoized for great efficiency.
*/
static map<unsigned int,unsigned int> _fib;
unsigned int fib(unsigned int i) {
    if (_fib.count(i) != NULL) {
        return _fib[i];
    } else {
        unsigned int ret = fib(i-1) + _fib[i-2];
        _fib[i] = ret;
        return ret;
    }
}
int main() {
    _fib[0] = 0;
    _fib[1] = 1;

    // some test cases
    assert(fib(0) == 0);
    assert(fib(1) == 1);

    // debug
    printf("_fib[2] is %u\n", _fib[2]);

    assert(fib(14) == 377);
}
