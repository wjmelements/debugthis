#include "assert.h"
#include "stdlib.h"

#ifndef NULL
#define NULL 0
#endif

#define MAX(x,y) (x > y ? x : y)

#include <vector>
using std::vector;

/*
    Returns the ith fibonacci number
    Memoized for great efficiency.
    _fib is a contiguous listing of calculated fibonacci numbers
*/
vector<unsigned int> _fib(2);
unsigned int fib(unsigned int i) {
    size_t new_size = MAX(_fib.size(),i);
    _fib.reserve(new_size);
    for (unsigned int curr = _fib.size(); curr < new_size; ++curr) {
        _fib.push_back(_fib[curr-2] + _fib[curr-1]);
    }
    return _fib[i];
}
int main() {
    // initialize
    _fib[0] = 0;
    _fib[1] = 1;
    // some test cases
    assert(fib(0) == 0);
    assert(fib(1) == 1);
    assert(fib(19) == 4181);
}
