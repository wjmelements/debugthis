#include "assert.h"
#include "math.h"

#include <set>
using std::set;

/*
    Returns whether a number is prime.
    primes is a contiguous list of known primes.
*/
static set<unsigned int> primes;
bool is_prime(unsigned int num) {
    if (primes.count(num)) {
        return true;
    }
    auto upper = primes.upper_bound(num);
    if (upper != primes.end()) {
        return false;
    }
    upper--;
    for (auto i = primes.cbegin(); i != primes.cend(); i++) {
        if (num % *i == 0) {
            return false;
        }
    }
    // upper is now the last element in the set
    for (unsigned int i = *upper; i <= sqrt(num); i++) {
        bool prime = true;
        for (auto it = primes.cbegin(); it != primes.cend(); it++) {
            if (i % *it == 0) {
                prime = false;
                break;
            }
        }
        if (prime) {
            // insert with hint
            upper = primes.insert(upper,i);
            if (num % i == 0) {
                return false;
            }
        }
    }
    // then num is prime
    primes.insert(upper,num);
    return true;
}
int main() {
    // initialize
    primes.insert(primes.insert(2).first,3);
    // test
    assert(is_prime(2));
    assert(is_prime(3));
    assert(not is_prime(4));
    assert(not is_prime(49));
    assert(is_prime(101));
    assert(is_prime(5));
    assert(is_prime(79));
    assert(is_prime(1451));
}
