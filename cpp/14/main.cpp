#include "assert.h"
#include "math.h"

#include <vector>
using std::vector;


/*
    Returns whether the sorted vector contains the element
*/
template <typename T> bool contains(vector<T>& nums, T val) {
    // lower inclusive bound
    unsigned int lower = 0;
    // upper exclusive bound
    unsigned int upper = nums.size();
    while (lower < upper) {
        unsigned int mid = (upper + lower) / 2;
        if (nums[mid] < val) {
            lower = mid + 1;
        } else {
            if (nums[mid] > val) {
                upper = mid - 1;
            } else {
                return true;
            }
        }
    }
    return false;
}

/*
    Returns whether a number is prime.
    primes is a contiguous sorted vector of primes
*/
static vector<unsigned int> primes;
bool is_prime(unsigned int num) {
    if (contains(primes,num)) {
        return true;
    }
    auto upper = primes.back();
    if (upper > num) {
        return false;
    }
    for (auto it = primes.cbegin(); it != primes.cend(); it++) {
        if (num % *it == 0) {
            return false;
        }
    }
    for (unsigned int i = upper + 1; i <= sqrt(num); i++) {
        bool prime = true;
        for (auto it = primes.cbegin(); it != primes.cend(); it++) {
            if (i % *it == 0) {
                prime = false;
                break;
            }
        }
        if (prime) {
            primes.push_back(i);
            if (num % i == 0) {
                return false;
            }
        }
    }
    return true;
}
int main() {
    // initialize
    primes.push_back(2);
    primes.push_back(3);
    // test
    assert(is_prime(2));
    assert(is_prime(3));
    assert(not is_prime(4));
    assert(not is_prime(26));
    assert(is_prime(5));
    assert(not is_prime(49));
    assert(is_prime(101));
    assert(is_prime(5));
    assert(is_prime(79));
    assert(not is_prime(143));
    assert(is_prime(1567));
}
