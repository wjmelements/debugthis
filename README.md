debugme
=======

Instructions
----
For the problem sets, make minimal changes to the code such that it is now correct. Weigh your success on how minimal those changes are. Complete rewrites are bad because the goal is to debug. Keep changes in the spirit of the original. If it was optimizing for some case, the result should keep that optimization.
